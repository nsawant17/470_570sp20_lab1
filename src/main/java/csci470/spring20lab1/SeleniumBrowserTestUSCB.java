package csci470.spring20lab1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumBrowserTestUSCB {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver, 10);

		try {
			// First, navigate to USCB main homepage
			

			// Use the USCB site's search bar to search for "computer science"
			

			// If proper SEO practices are followed, then USCB's computer science 
			// department should be the first result (but just in case, try to find 
			// the CSS selector that correctly identifies the first non-ad hyperlink 
			// in the search results)
			/*
			WebElement firstResult = wait.until(
					presenceOfElementLocated(By.cssSelector("")));
			if ( ) {
				System.out.println(
						"... Passed.");
			} else {
				System.out.println(
						"... Failed.");
			}
			*/

			// Assuming the link was found, visit the link
			

			// After the browser has navigated to the above link,
			// validate the page title
			/*
			if (driver.getTitle().contains("")) {
				System.out.println("... Passed.");
			} else {
				System.out.println("... Failed.");
			}
			*/

			// Now try the first link under Computer Science in the left menu
			/*
			WebElement link1 = driver.findElement(
					By.linkText());
			driver.navigate().to();
			*/
			
			// Validate Page Title
			/*
			if (driver.getTitle().contains("")) {
				System.out.println("... Passed.");
			} else {
				System.out.println("... Failed.");
			}
			*/

			// Simulate "clicking" of the first tab and check to 
			// make sure that the tab's display attribute is visible 
			/*
			driver.findElement(By.cssSelector("")).click();
			if (driver.findElement(By.cssSelector("")).isDisplayed()) {
				System.out.println("... Passed.");
			} else {
				System.out.println("... Failed.");
			}
			*/

			// On your own: adapt the above code for the second tab
			

			// On your own: adapt the above code for the third tab
			

			// On your own: adapt the above code for the fourth tab
			
			
			
			// On your own: 
			// See if clicking "Apply Now" launches application in new tab
			// Cited reference: https://stackoverflow.com/a/16541981 
			/*
			String oldTab = driver.getWindowHandle();
			driver.findElement(By.linkText("")).click();
			ArrayList<String> newTab = 
					new ArrayList<String>(driver.getWindowHandles());
			newTab.remove(oldTab);
			// change focus to new tab
			driver.switchTo().window(newTab.get(0));
			if (driver.getTitle().contains("")) {
				System.out.println(
						"... Passed.");
			} else {
				System.out.println(
						"... Failed.");
			}
			*/
			
		} finally {
			// quit the browser when tests have finished or if exception thrown 
			System.out.println("Closing browser...");
			driver.quit();
		}

	} // end method main

} // end class SeleniumBrowserTestUSCB